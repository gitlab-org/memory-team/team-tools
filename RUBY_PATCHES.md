# Overview

It is difficult to get deeper insight into Ruby VM behavior without certain compile time switches being enabled.

This document summarizes switches I found to be useful. These switches need to be enabled in source code via patch files.

## `USE_DEBUG_COUNTER`

This will enable Ruby debug counters. Debug counters count VM-internal events that are of interest especially in
performance monitoring such as:

- GC and heap event and size counters
- object types counters, including breakdowns by size and heap area they occupy
- ivar and stack frame events
- JIT events

Debug counters are collected during VM runtime, and dumped to `stderr` on VM exit.
<details>
<summary>
Here is the output Ruby 2.7.2 produces after exiting from a Rails console:
<pre><code>
[RUBY_DEBUG_COUNTER]	109 normal exit.
[RUBY_DEBUG_COUNTER]	mc_inline_hit                 	    16,823,336
[RUBY_DEBUG_COUNTER]	mc_inline_miss                	     2,098,993
[RUBY_DEBUG_COUNTER]	mc_global_hit                 	     6,362,224
...
</code></pre>
</summary>
<strong>Full output:</strong>
<pre><code>
[RUBY_DEBUG_COUNTER]	109 normal exit.
[RUBY_DEBUG_COUNTER]	mc_inline_hit                 	    16,823,336
[RUBY_DEBUG_COUNTER]	mc_inline_miss                	     2,098,993
[RUBY_DEBUG_COUNTER]	mc_global_hit                 	     6,362,224
[RUBY_DEBUG_COUNTER]	mc_global_miss                	       774,862
[RUBY_DEBUG_COUNTER]	mc_global_state_miss          	       244,576
[RUBY_DEBUG_COUNTER]	mc_class_serial_miss          	     1,854,417
[RUBY_DEBUG_COUNTER]	mc_cme_complement             	       737,934
[RUBY_DEBUG_COUNTER]	mc_cme_complement_hit         	       719,505
[RUBY_DEBUG_COUNTER]	mc_search_super               	    31,668,699
[RUBY_DEBUG_COUNTER]	mc_miss_by_nome               	         3,322
[RUBY_DEBUG_COUNTER]	mc_miss_by_distinct           	     1,192,577
[RUBY_DEBUG_COUNTER]	mc_miss_by_refine             	             0
[RUBY_DEBUG_COUNTER]	mc_miss_by_visi               	       117,635
[RUBY_DEBUG_COUNTER]	mc_miss_spurious              	       785,459
[RUBY_DEBUG_COUNTER]	mc_miss_reuse_call            	       640,440
[RUBY_DEBUG_COUNTER]	ccf_general                   	     1,052,090
[RUBY_DEBUG_COUNTER]	ccf_iseq_setup                	     2,899,691
[RUBY_DEBUG_COUNTER]	ccf_iseq_setup_0start         	        83,268
[RUBY_DEBUG_COUNTER]	ccf_iseq_setup_tailcall_0start	             0
[RUBY_DEBUG_COUNTER]	ccf_iseq_fix                  	     4,636,167
[RUBY_DEBUG_COUNTER]	ccf_iseq_opt                  	       247,441
[RUBY_DEBUG_COUNTER]	ccf_iseq_kw1                  	        29,098
[RUBY_DEBUG_COUNTER]	ccf_iseq_kw2                  	        13,224
[RUBY_DEBUG_COUNTER]	ccf_cfunc                     	     6,824,506
[RUBY_DEBUG_COUNTER]	ccf_ivar                      	     2,205,347
[RUBY_DEBUG_COUNTER]	ccf_attrset                   	       124,608
[RUBY_DEBUG_COUNTER]	ccf_method_missing            	         4,955
[RUBY_DEBUG_COUNTER]	ccf_zsuper                    	             0
[RUBY_DEBUG_COUNTER]	ccf_bmethod                   	       108,083
[RUBY_DEBUG_COUNTER]	ccf_opt_send                  	       533,736
[RUBY_DEBUG_COUNTER]	ccf_opt_call                  	       427,951
[RUBY_DEBUG_COUNTER]	ccf_opt_block_call            	            26
[RUBY_DEBUG_COUNTER]	ccf_super_method              	       699,509
[RUBY_DEBUG_COUNTER]	frame_push                    	    20,139,571
[RUBY_DEBUG_COUNTER]	frame_push_method             	     7,908,889
[RUBY_DEBUG_COUNTER]	frame_push_block              	     2,660,580
[RUBY_DEBUG_COUNTER]	frame_push_class              	        28,437
[RUBY_DEBUG_COUNTER]	frame_push_top                	        10,108
[RUBY_DEBUG_COUNTER]	frame_push_cfunc              	     8,578,860
[RUBY_DEBUG_COUNTER]	frame_push_ifunc              	       892,100
[RUBY_DEBUG_COUNTER]	frame_push_eval               	        35,290
[RUBY_DEBUG_COUNTER]	frame_push_rescue             	        25,299
[RUBY_DEBUG_COUNTER]	frame_push_dummy              	             8
[RUBY_DEBUG_COUNTER]	frame_R2R                     	     7,565,662
[RUBY_DEBUG_COUNTER]	frame_R2C                     	     8,043,253
[RUBY_DEBUG_COUNTER]	frame_C2C                     	     1,427,707
[RUBY_DEBUG_COUNTER]	frame_C2R                     	     3,102,941
[RUBY_DEBUG_COUNTER]	ivar_get_ic_hit               	     5,201,187
[RUBY_DEBUG_COUNTER]	ivar_get_ic_miss              	       446,829
[RUBY_DEBUG_COUNTER]	ivar_get_ic_miss_serial       	       375,651
[RUBY_DEBUG_COUNTER]	ivar_get_ic_miss_unset        	       294,054
[RUBY_DEBUG_COUNTER]	ivar_get_ic_miss_noobject     	             0
[RUBY_DEBUG_COUNTER]	ivar_set_ic_hit               	       579,531
[RUBY_DEBUG_COUNTER]	ivar_set_ic_miss              	       524,167
[RUBY_DEBUG_COUNTER]	ivar_set_ic_miss_serial       	       256,791
[RUBY_DEBUG_COUNTER]	ivar_set_ic_miss_unset        	        45,397
[RUBY_DEBUG_COUNTER]	ivar_set_ic_miss_oorange      	       171,404
[RUBY_DEBUG_COUNTER]	ivar_set_ic_miss_noobject     	        50,575
[RUBY_DEBUG_COUNTER]	ivar_get_base                 	       613,406
[RUBY_DEBUG_COUNTER]	ivar_set_base                 	       659,129
[RUBY_DEBUG_COUNTER]	lvar_get                      	    24,298,172
[RUBY_DEBUG_COUNTER]	lvar_get_dynamic              	     2,635,758
[RUBY_DEBUG_COUNTER]	lvar_set                      	     4,163,887
[RUBY_DEBUG_COUNTER]	lvar_set_dynamic              	        17,862
[RUBY_DEBUG_COUNTER]	lvar_set_slowpath             	        12,736
[RUBY_DEBUG_COUNTER]	gc_count                      	            61
[RUBY_DEBUG_COUNTER]	gc_minor_newobj               	            33
[RUBY_DEBUG_COUNTER]	gc_minor_malloc               	            13
[RUBY_DEBUG_COUNTER]	gc_minor_method               	             0
[RUBY_DEBUG_COUNTER]	gc_minor_capi                 	             0
[RUBY_DEBUG_COUNTER]	gc_minor_stress               	             0
[RUBY_DEBUG_COUNTER]	gc_major_nofree               	             9
[RUBY_DEBUG_COUNTER]	gc_major_oldgen               	             1
[RUBY_DEBUG_COUNTER]	gc_major_shady                	             1
[RUBY_DEBUG_COUNTER]	gc_major_force                	             0
[RUBY_DEBUG_COUNTER]	gc_major_oldmalloc            	             8
[RUBY_DEBUG_COUNTER]	gc_isptr_trial                	       488,491
[RUBY_DEBUG_COUNTER]	gc_isptr_range                	       185,585
[RUBY_DEBUG_COUNTER]	gc_isptr_align                	       115,051
[RUBY_DEBUG_COUNTER]	gc_isptr_maybe                	       104,185
[RUBY_DEBUG_COUNTER]	obj_newobj                    	     7,205,943
[RUBY_DEBUG_COUNTER]	obj_newobj_slowpath           	        51,312
[RUBY_DEBUG_COUNTER]	obj_newobj_wb_unprotected     	       323,627
[RUBY_DEBUG_COUNTER]	obj_free                      	     5,371,389
[RUBY_DEBUG_COUNTER]	obj_promote                   	     1,779,301
[RUBY_DEBUG_COUNTER]	obj_wb_unprotect              	        40,834
[RUBY_DEBUG_COUNTER]	obj_obj_embed                 	        55,426
[RUBY_DEBUG_COUNTER]	obj_obj_transient             	        33,632
[RUBY_DEBUG_COUNTER]	obj_obj_ptr                   	         9,679
[RUBY_DEBUG_COUNTER]	obj_str_ptr                   	       644,284
[RUBY_DEBUG_COUNTER]	obj_str_embed                 	     1,595,887
[RUBY_DEBUG_COUNTER]	obj_str_shared                	       313,996
[RUBY_DEBUG_COUNTER]	obj_str_nofree                	             0
[RUBY_DEBUG_COUNTER]	obj_str_fstr                  	        20,293
[RUBY_DEBUG_COUNTER]	obj_ary_embed                 	       861,313
[RUBY_DEBUG_COUNTER]	obj_ary_transient             	       146,718
[RUBY_DEBUG_COUNTER]	obj_ary_ptr                   	        32,395
[RUBY_DEBUG_COUNTER]	obj_ary_extracapa             	        64,485
[RUBY_DEBUG_COUNTER]	obj_ary_shared_create         	        40,790
[RUBY_DEBUG_COUNTER]	obj_ary_shared                	        20,131
[RUBY_DEBUG_COUNTER]	obj_ary_shared_root_occupied  	        11,831
[RUBY_DEBUG_COUNTER]	obj_hash_empty                	       113,116
[RUBY_DEBUG_COUNTER]	obj_hash_1                    	        83,236
[RUBY_DEBUG_COUNTER]	obj_hash_2                    	        50,863
[RUBY_DEBUG_COUNTER]	obj_hash_3                    	        18,247
[RUBY_DEBUG_COUNTER]	obj_hash_4                    	         3,661
[RUBY_DEBUG_COUNTER]	obj_hash_5_8                  	        10,311
[RUBY_DEBUG_COUNTER]	obj_hash_g8                   	        12,774
[RUBY_DEBUG_COUNTER]	obj_hash_null                 	        96,008
[RUBY_DEBUG_COUNTER]	obj_hash_ar                   	       141,614
[RUBY_DEBUG_COUNTER]	obj_hash_st                   	        54,586
[RUBY_DEBUG_COUNTER]	obj_hash_transient            	       141,191
[RUBY_DEBUG_COUNTER]	obj_hash_force_convert        	             0
[RUBY_DEBUG_COUNTER]	obj_struct_embed              	        15,573
[RUBY_DEBUG_COUNTER]	obj_struct_transient          	           108
[RUBY_DEBUG_COUNTER]	obj_struct_ptr                	             3
[RUBY_DEBUG_COUNTER]	obj_data_empty                	         8,080
[RUBY_DEBUG_COUNTER]	obj_data_xfree                	       247,631
[RUBY_DEBUG_COUNTER]	obj_data_imm_free             	       113,410
[RUBY_DEBUG_COUNTER]	obj_data_zombie               	           543
[RUBY_DEBUG_COUNTER]	obj_match_under4              	        67,552
[RUBY_DEBUG_COUNTER]	obj_match_ge4                 	         5,090
[RUBY_DEBUG_COUNTER]	obj_match_ge8                 	            40
[RUBY_DEBUG_COUNTER]	obj_match_ptr                 	        72,682
[RUBY_DEBUG_COUNTER]	obj_iclass_ptr                	        14,270
[RUBY_DEBUG_COUNTER]	obj_class_ptr                 	        13,080
[RUBY_DEBUG_COUNTER]	obj_module_ptr                	         2,593
[RUBY_DEBUG_COUNTER]	obj_bignum_ptr                	             0
[RUBY_DEBUG_COUNTER]	obj_bignum_embed              	           231
[RUBY_DEBUG_COUNTER]	obj_float                     	             0
[RUBY_DEBUG_COUNTER]	obj_complex                   	             1
[RUBY_DEBUG_COUNTER]	obj_rational                  	         2,069
[RUBY_DEBUG_COUNTER]	obj_regexp_ptr                	         1,960
[RUBY_DEBUG_COUNTER]	obj_file_ptr                  	         2,116
[RUBY_DEBUG_COUNTER]	obj_symbol                    	           670
[RUBY_DEBUG_COUNTER]	obj_imemo_ment                	        60,433
[RUBY_DEBUG_COUNTER]	obj_imemo_iseq                	        77,647
[RUBY_DEBUG_COUNTER]	obj_imemo_env                 	        83,567
[RUBY_DEBUG_COUNTER]	obj_imemo_tmpbuf              	        65,999
[RUBY_DEBUG_COUNTER]	obj_imemo_ast                 	        58,740
[RUBY_DEBUG_COUNTER]	obj_imemo_cref                	        37,812
[RUBY_DEBUG_COUNTER]	obj_imemo_svar                	       103,184
[RUBY_DEBUG_COUNTER]	obj_imemo_throw_data          	        94,029
[RUBY_DEBUG_COUNTER]	obj_imemo_ifunc               	       129,163
[RUBY_DEBUG_COUNTER]	obj_imemo_memo                	        98,413
[RUBY_DEBUG_COUNTER]	obj_imemo_parser_strterm      	        70,131
[RUBY_DEBUG_COUNTER]	artable_hint_hit              	     1,280,446
[RUBY_DEBUG_COUNTER]	artable_hint_miss             	        20,642
[RUBY_DEBUG_COUNTER]	artable_hint_notfound         	       917,615
[RUBY_DEBUG_COUNTER]	heap_xmalloc                  	     4,171,350
[RUBY_DEBUG_COUNTER]	heap_xrealloc                 	       357,820
[RUBY_DEBUG_COUNTER]	heap_xfree                    	     3,777,159
[RUBY_DEBUG_COUNTER]	theap_alloc                   	       625,394
[RUBY_DEBUG_COUNTER]	theap_alloc_fail              	             1
[RUBY_DEBUG_COUNTER]	theap_evacuate                	       148,326
[RUBY_DEBUG_COUNTER]	mjit_exec                     	             0
[RUBY_DEBUG_COUNTER]	mjit_exec_not_added           	             0
[RUBY_DEBUG_COUNTER]	mjit_exec_not_added_add_iseq  	             0
[RUBY_DEBUG_COUNTER]	mjit_exec_not_ready           	             0
[RUBY_DEBUG_COUNTER]	mjit_exec_not_compiled        	             0
[RUBY_DEBUG_COUNTER]	mjit_exec_call_func           	             0
[RUBY_DEBUG_COUNTER]	mjit_frame_VM2VM              	             0
[RUBY_DEBUG_COUNTER]	mjit_frame_VM2JT              	             0
[RUBY_DEBUG_COUNTER]	mjit_frame_JT2JT              	             0
[RUBY_DEBUG_COUNTER]	mjit_frame_JT2VM              	             0
[RUBY_DEBUG_COUNTER]	mjit_cancel                   	             0
[RUBY_DEBUG_COUNTER]	mjit_cancel_ivar_inline       	             0
[RUBY_DEBUG_COUNTER]	mjit_cancel_send_inline       	             0
[RUBY_DEBUG_COUNTER]	mjit_cancel_opt_insn          	             0
[RUBY_DEBUG_COUNTER]	mjit_cancel_invalidate_all    	             0
[RUBY_DEBUG_COUNTER]	mjit_length_unit_queue        	             0
[RUBY_DEBUG_COUNTER]	mjit_length_active_units      	             0
[RUBY_DEBUG_COUNTER]	mjit_length_compact_units     	             0
[RUBY_DEBUG_COUNTER]	mjit_length_stale_units       	             0
[RUBY_DEBUG_COUNTER]	mjit_compile_failures         	             0
</code></pre>
</details>

## `MALLOC_ALLOCATED_SIZE` and `USE_GC_MALLOC_OBJ_INFO_DETAILS`

I grouped these together because they are closely related and often work in tandem. These switches enable counters
concerned with tracing malloc behavior, such as invocation counts and bytes allocated, using various break downs.
As with debug counters, they are collected during the entire application lifetime and dumped on exit. `MALLOC_ALLOCATED_SIZE`
additionally exposes two of these counters as functions on `GC`:

- `GC.malloc_allocated_size`: size in bytes of all memory allocated by malloc
- `GC.malloc_allocations`: invocation count of malloc

For instance, in a fresh Rails console, we can see that we allocated 416MB via malloc across 2.5M invocations:

```ruby
[2] pry(main)> GC.malloc_allocated_size
=> 416781952
[3] pry(main)> GC.malloc_allocations
=> 2577088
```

On exit, a number of counters will be printed, explained below.

### Generational `malloc` histogram

*My notes are based on https://bugs.ruby-lang.org/issues/14857*

The first block represents object life-time as a histogram, measured in "GC cycles survived". There are three columns:

- Object age as the number of GCs it survived: if an object created at "rb_gc_count() == 30" is freed at 33, then its age is 3 (33-30).
- malloc count for this age group
- malloc bytes for this age group

This can be used to learn to what degree an application actually follows the generational hypothesis ("most objects die young")
and where an application experiences the most malloc or GC churn. Below is the output from our Rails console:
 
```
* malloc_info gen statistics
0	1880639	1152537744
1	1783221	447951984
2	9589	5361376
3	4025	10669664
4	3144	1554880
5	741	451840
6	2661	2798128
7	4909	11307680
8	5543	762352
9	12548	10987536
10	6672	557152
11	699	338736
12	585	197200
13	3497	20203280
14	10128	853168
15	2313	249536
16	779	628128
17	155	12976
18	1938	5240384
19	2499	295952
20	1060	221232
21	720	58176
22	612	53296
23	798	80880
24	1517	125680
25	439	308672
26	239	21328
27	1098	107344
28	131	18960
29	1	64
30	0	0
31	12	3632
32	48	14480
33	0	0
...
more	0	0
```

The oldest objects survived 32 GC runs before being freed, but only accounted for a very small portion of the object space.
Most objects in terms of malloc count and total malloc'ed bytes died between two GC runs (age 0), so at least in idle
state our application confirms the generational hypothesis.

### `malloc` size histogram

The second block is a histogram of malloc allocation size distribution. Each group has a bucket size of `2 * previous_bucket`.

For example, running the Rails console shows that most mallocs (1.7M) can be attributed to objects between 65 and 128 bytes in size:

```
* malloc_info size statistics
16	4
32	0
64	1050715
128	1760900
256	250807
512	197230
1024	306404
2048	129383
4096	16590
8192	11944
more	19522
```

It looks like we have a long tail of objects that are quite large, more than 1kB in size. It is not clear from this histogram, however,
whether these objects were freed again or are long-lived.

### `malloc` by-file histogram

The last block is a histogram counting malloc calls and malloc bytes by source file:


```
* malloc_info file statistics
st.c	271737	466460544
id_table.c	65197	10665472
class.c	103398	7353424
string.c	708104	104765472
iseq.c	269828	147918720
iseq.h	96209	18472128
compile.c	560765	99007680
array.c	33175	360316720
util.c	10887	1089904
parse.y	701586	61374400
io.c	3410	13854752
gc.c	630745	192748960
node.c	58744	120307712
vm_method.c	19704	1576320
re.c	71608	5729216
hash.c	700	112000
vm.c	78441	6209696
vm_backtrace.c	25693	46550976
variable.c	8801	575776
object.c	7055	566400
error.c	2	128
file.c	2258	433536
(null)	620	111520
missing/dtoa.c	73	4672
struct.c	2	320
transcode.c	460	624160
../fbuffer/fbuffer.h	9	1792
raddrinfo.c	4	224
thread.c	25	4000
../fbuffer/fbuffer.h	14220	7201488
process.c	2	192
zlib.c	11	481280
internal.h	2	864
signal.c	3	61440
eval_jump.c	6	384
generator.c	8	384
bigdecimal.c	2	160
stringio.c	4	320
```

It looks like we allocate 466MB of heap space just for hash tables. I believe they are used heavily by Ruby internally as well,
so this isn't just our own application contributing to this.
