# GitLab performance testing cheat sheet

These are the recommended manual steps to perform in order to run a performance test with `gpt`.

We should aim to automate these steps bit by bit so that tests become easier to run results can
be easily compared.

## Step by step instructions

### Create VM instance

Instantiate new VM from this template: https://console.cloud.google.com/compute/instanceTemplates/details/gpt-omnibus?project=group-memory-testbed-c2c979

Options:
- Machine: `n2-highcpu-4` (vary if necessary)
- Zone: `europe-west3-c` (Frankfurt)

### Update Omnibus settings

New settings:

```ruby
external_url 'http://<ip>'

puma['worker_processes'] = 0 # run in single-mode
letsencrypt['enable'] = false

gitlab_rails['env'] = {
    'GITLAB_CHAOS_SECRET' => 'x',
    # other env vars you want to test, such as RUBY_GC_* settings
}
```

We disable Let's Encrypt because otherwise `reconfigure` will fail as the endpoint is HTTP.

Save and apply via `gitlab-ctl reconfigure`.

### Create gpt environment settings

create `k6/config/environments/gcp.json` as:

```json
{
  "environment": {
    "name": "gcp",
    "url": "<external_host_setting_from_above>",
    "user": "root",
    "config": {
      "latency": "30"
    },
    "storage_nodes": ["default"]
  },
  "gpt_data": {
    "root_group": "gpt",
    "large_projects": {
      "group": "large_projects",
      "project": "gitlabhq"
    },
    "many_groups_and_projects": {
      "group": "many_groups_and_projects",
      "subgroups": 5,
      "subgroup_prefix": "gpt-subgroup-",
      "projects": 5,
      "project_prefix": "gpt-project-"
    }
  }
}
```
#### Access token

```
export ACCESS_TOKEN=<token>
```

The VM/template description will list an access token you can use.
Of course you are free to issue a new one with your own instance.

### Run perfomance test suite

```
bundle exec bin/run-k6 --environment gcp.json --options 5s_2rps.json
```

### Capture results

#### RSS

RSS is best captured over time and as a final average via `pidstat`:

```bash
pidstat -r -p $(pgrep -f 'puma 5.1.1') 1 | tee pidstat.out
```

The columnar output also makes this easy to copy-and-paste into a GSheet.

#### USS

USS/PSS can be measured most easily via `smem`:

```bash
watch -n 1 smem -U git -P 'puma 5.1.1'
```

#### gpt reports

The easiest way is to grab the test result summary from `results/<test_run>.csv` and paste it into a GSheet via the "split-to-columns" paste option.

#### Ruby GC metrics

When the test has finished, run a GC cycle and pull GC stats as CSV:

```bash
curl -XPOST -H'X-Chaos-Secret: x' <host-or-ip>/-/chaos/gc | jq '.gc_stat | to_entries | map({metric: .key, value: .value})' | jq -r '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
```

Again, copy and paste into a GSheet as before for easy processing and sharing.
