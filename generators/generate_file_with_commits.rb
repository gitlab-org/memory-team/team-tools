# frozen_string_literal: true

# A file generator script that will generate file with a provided
# number of lines and commits.
#
# It was used to generate large files for measuring test performance
# for Gitlab Blame functionality.
#
# For each generated line, it will create separate commit.
#
# It will push the created file to your repo in the end.
#
# - full_path: path to the file we would like
#     to generate inside existing git repository
# - number_of_lines: number of lines/commits to be generated
#
# Run with:
#
# ```sh
# ruby generate_file_with_commits.rb :full_path :number_of_lines
# ```

require 'securerandom'

if ARGV.length != 2
  puts 'Please provide full_path and number_of_lines'
  exit 1
end
path = ARGV[0]
number_of_lines = ARGV[1]

dirname = File.dirname(path)
puts "DIRNAME:#{dirname}"

puts 'Please provide existing file path' unless File.exist?(dirname)

puts "Generating #{number_of_lines.to_i} commits for: #{path} file"

number_of_lines.to_i.times do |i|
  File.open(path, 'a') do |f|
    f << "- #{SecureRandom.alphanumeric(rand(1..255))}\n"
  end

  Dir.chdir(dirname) do
    `git add . -A`
    `git commit -m "Add new line #{i + 1}"`

    printf("\rGenerate and commit: %<line>d\\%<total>d lines", line: i + 1, total: number_of_lines)

    if i == number_of_lines.to_i - 1
      puts "\nPushing to git"
      `git push`
      sleep 1
      `git gc --prune`
    end
  end
end
